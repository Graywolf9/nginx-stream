KEY="5249998851692381?s_bl=1&s_psm=1&s_sc=5249998908359042&s_sw=0&s_vt=api-s&a=Abzr2WirFjdNFlJT"

VIDEO=$1

if [ -n "$2" ]; then
    echo "=========";
    echo "Start at "$2;
    echo "=========";
    TIEMPO="-ss $2";
fi

ffmpeg -re $TIEMPO -i $VIDEO\
 -acodec copy -vcodec copy -f flv \
 "rtmps://live-api-s.facebook.com:443/rtmp/"$KEY
