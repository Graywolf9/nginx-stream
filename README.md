# nginx-stream

Dockerfiles for an nginx server with rtmp module, stunnel and configured

## Send to
url: rtmp://localhost/live
streamkey: streamname

## hls url
url: http://localhost/live/streamname.m3u8

## view hls
url: http://localhost/vivo

### Don't forget change the <KEY> values in nginx.conf for your socialmedia stream key
