FROM debian:10.5-slim

RUN apt update

RUN apt-get install build-essential libpcre3 libpcre3-dev libssl-dev -y

RUN apt install wget unzip zlib1g-dev stunnel4 -y

WORKDIR /workdir

RUN wget http://nginx.org/download/nginx-1.15.1.tar.gz

RUN wget https://github.com/sergey-dryabzhinsky/nginx-rtmp-module/archive/dev.zip

RUN tar -zxvf nginx-1.15.1.tar.gz

RUN unzip dev.zip

WORKDIR /workdir/nginx-1.15.1

RUN ./configure --with-http_ssl_module --add-module=../nginx-rtmp-module-dev

RUN make

RUN make install

#CMD tail -f /dev/null

CMD /usr/local/nginx/sbin/nginx && service stunnel4 start && tail -f /usr/local/nginx/logs/access.log
